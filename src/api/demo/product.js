import request from '@/utils/request'

// 查询产品树表（mb）列表
export function listProduct(query) {
  return request({
    url: '/demo/product/list',
    method: 'get',
    params: query
  })
}

// 查询产品树表（mb）详细
export function getProduct(productId) {
  return request({
    url: '/demo/product/' + productId,
    method: 'get'
  })
}

// 新增产品树表（mb）
export function addProduct(data) {
  return request({
    url: '/demo/product',
    method: 'post',
    data: data
  })
}

// 修改产品树表（mb）
export function updateProduct(data) {
  return request({
    url: '/demo/product',
    method: 'put',
    data: data
  })
}

// 删除产品树表（mb）
export function delProduct(productId) {
  return request({
    url: '/demo/product/' + productId,
    method: 'delete'
  })
}
